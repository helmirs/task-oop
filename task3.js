/**
 * 1. Gunakan fungsi konstruktor untuk mengimplementasikan Mobil Listrik (called 'EVCl') sebagai child "kelas" dari 'CarCl'.
 * Selain kecepatan merek dan saat ini, 'EV' juga memiliki daya baterai saat ini dalam % (properti 'charge')
 * 2. Terapkan metode 'chargeBattery' yang membutuhkan argumen 'chargeTo' dan menyetel daya baterai ke 'chargeTo'
 * 3. Jadikan property 'charge' private
 * 4. Implement the ability to chain the 'accelerate' and 'chargeBattery' methods of this class, and also update the 'brake' method in the 'CarCl' class.
 */

class CarCl {
  constructor(make, speed) {
    this.make = make;
    this.speed = speed;
  }

  eccelerate() {
    this.speed += 20;
    console.log(`${this.make} is going at ${this.speed} km/h`);
  }

  brake() {
    this.speed -= 5;
    console.log(`${this.make} is going at ${this.speed} km/h`);
    return this;
  }

  get speedUS() {
    return this.speed / 1.6;
  }

  set speedUS(speed) {
    this.speed = speed * 1.6;
  }
}

class EVCl extends CarCl {
  /**
   * Your code here (for private property)
   */

  constructor(make, speed, charge) {
    super(make, speed);
    this.#charge = charge;
  }

  chargeBattery(chargeTo) {
    this.#charge = chargeTo;
    return this;
  }

  accelerate() {
    this.speed += 20;
    this.#charge--;
    /**
     * Your code here (accelerate kecepatan)
     * Your code here (charge berkurang)
     */
    console.log(
      `${this.make} is going at ${this.speed} km/h, with a charge of ${
        this.#charge
      }`
    );
    return this;
  }
}

/**
 * Your code here (inisiasi with keyword new)
 */

console.log(tesla);
// console.log(tesla.#charge);
tesla
  .accelerate()
  .accelerate()
  .accelerate()
  .brake()
  .chargeBattery(50)
  .accelerate();

console.log(tesla.speedUS);

/**
 * Output:
 * Tesla is going at 140 km/h, with a charge 22
 * Tesla is going at 160 km/h, with a charge 21
 * Tesla is going at 180 km/h, with a charge 20
 * Tesla is going at 175 km/h
 * Tesla is going at 195 km/h, with charge of 49
 * 121.875
 */
